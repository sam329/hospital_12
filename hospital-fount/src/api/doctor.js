import request from '@/utils/request'

export function updateCommentsReply(data) {
  return request({
    url: '/doctorMain/updateCommentsReply',
    method: 'post',
    data
  })
}

export function getCommentsByDoctorId(data) {
  return request({
    url: `/doctorMain/getCommentsByDoctorId/${data}`,
    method: 'post'
  })
}

export function register(data) {
  return request({
    url: '/doctor/register',
    method: 'post',
    data
  })
}

export function login(data) {
  return request({
    url: '/doctor/login',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/doctor/update',
    method: 'put',
    data
  })
}
export function updatePwd(data) {
  return request({
    url: '/doctor/updatePwd',
    method: 'put',
    data
  })
}
export function getdoctor(data) {
  return request({
    url: '/doctor/getdoctor',
    method: 'get',
    data
  })
}
