import request from '@/utils/request'

export function register(data) {
  return request({
    url: '/user/register',
    method: 'post',
    data
  })
}
export function getUser(data) {
  return request({
    url: '/user/info',
    method: 'get',
    data
  })
}

export function updatePwd(data) {
  return request({
    url: '/user/updatePwd',
    method: 'put',
    data
  })
}
export function updateInfo(data) {
  return request({
    url: '/user/updateInfo',
    method: 'put',
    data
  })
}

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'get'
  })
}
