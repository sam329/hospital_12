import request from '@/utils/request'
const groupName = 'link'
export function save(data) {
  return request({
    url: `/${groupName}/save`,
    method: 'post',
    data
  })
}

export function get(data) {
  return request({
    url: `/${groupName}/get/${data}`,
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: `/${groupName}/update`,
    method: 'put',
    data
  })
}

export function deleteById(data) {
  return request({
    url: `/${groupName}/delete/${data}`,
    method: 'delete'
  })
}

export function list() {
  return request({
    url: `/${groupName}/list`,
    method: 'get'
  })
}
