import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/views/userLayout'
import doctorLayout from '@/views/doctorLayout'
import sysUserLayout from '@/views/sysUserLayout'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: '/main',
    children: [
      {
        path: '/main',
        name: 'main',
        component: () => import('@/views/main')
      },
      {
        path: '/aboutO',
        name: 'aboutO',
        component: () => import('@/views/about/about')
      },
      {
        path: '/appointment',
        name: 'appointment',
        component: () => import('@/views/user-main/appointment')
      },
      {
        path: '/myOrder',
        name: 'myOrder',
        component: () => import('@/views/user-main/myOrder')
      },
      {
        path: '/myOrderGo',
        name: 'myOrderGo',
        component: () => import('@/views/user-main/myOrder')
      },
      {
        path: '/userEmail',
        name: 'userEmail',
        component: () => import('@/views/user-main/userEmail')
      },
      {
        path: '/selectDoctor',
        name: 'myOrderGo',
        component: () => import('@/views/user-main/selectDoctor')
      },
      {
        path: '/userCenter',
        name: 'userCenter',
        component: () => import('@/views/user-center')
      }
    ]
  }, {
    name: 'userlogin',
    path: '/userlogin',
    component: () => import('@/views/login/user')
  }, {
    name: 'doctorlogin',
    path: '/doctorlogin',
    component: () => import('@/views/login/doctor')
  }, {
    name: 'sysUserlogin',
    path: '/sysUserlogin',
    component: () => import('@/views/login/sysUser')
  }, {
    path: '/doctorMain',
    name: 'doctorLayout',
    component: doctorLayout,
    redirect: '/mainS',
    children: [
      {
        path: '/mainS',
        name: 'main',
        component: () => import('@/views/main')
      },
      {
        path: '/doctorEmail',
        name: 'doctorEmail',
        component: () => import('@/views/doctor-main/doctorEmail')
      },
      {
        path: '/about',
        name: 'about',
        component: () => import('@/views/about/about')
      },
      {
        path: '/userCenter',
        name: 'userCenter',
        component: () => import('@/views/user-center')
      }
    ]
  }, {
    path: '/sysUserMain',
    name: 'sysUserLayout',
    component: sysUserLayout,
    redirect: '/mainSS',
    children: [
      {
        path: '/mainSS',
        name: 'main',
        component: () => import('@/views/main')
      },
      {
        path: '/userList',
        name: 'userList',
        component: () => import('@/views/sysUser-main/userList')
      },
      {
        path: '/doctorList',
        name: 'doctorList',
        component: () => import('@/views/sysUser-main/doctorList')
      },
      {
        path: '/orderList',
        name: 'orderList',
        component: () => import('@/views/sysUser-main/orderList')
      },
      {
        path: '/about',
        name: 'about',
        component: () => import('@/views/about/about')
      },
      {
        path: '/weixin',
        name: 'weixin',
        component: () => import('@/views/weixin/weixin')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫,控制页面访问权限
router.beforeEach((to, from, next) => {
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (to.path === '/userlogin') {
    if (tokenStr != null) {
      alert('您已登陆，请退出')
      return null
    } else {
      next()
    }
  }
  if (to.path === '/userCenter' || to.path === '/appointment' || to.path === '/myOrder' || to.path === '/userEmail') {
    if (!tokenStr) {
      alert('请先登录')
      // return next('/userlogin')
      return null
    }
    next()
  }
  if (to.path === '/myOrderGo') {
    alert('您已填写完成，请前往查看并支付')
    next()
  }
  if (to.path === '/userlogout') {
    window.sessionStorage.clear()
    next()
  }
  next()
})

export default router
