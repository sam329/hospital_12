package com.fan.hospital.service;

import com.fan.hospital.pojo.Comments;
import com.fan.hospital.pojo.vo.CommentsVo;

import java.util.List;

public interface DoctorMainService {
    List<CommentsVo> getCommentsByDoctorId(long doctorId);
    void updateCommentsReply(Comments comments);
}
