package com.fan.hospital.service.impl;

import com.fan.hospital.mapper.DoctorMainMapper;
import com.fan.hospital.pojo.Comments;
import com.fan.hospital.pojo.vo.CommentsVo;
import com.fan.hospital.service.DoctorMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DoctorMainServiceImpl implements DoctorMainService {
    @Autowired
    private DoctorMainMapper doctorMainMapper;

    @Override
    public List<CommentsVo> getCommentsByDoctorId(long doctorId) {
        return doctorMainMapper.getCommentsByDoctorId(doctorId);
    }

    @Override
    public void updateCommentsReply(Comments comments) {
        comments.setReplyTime(new Date());
        doctorMainMapper.updateCommentsReply(comments);
    }
}
