package com.fan.hospital.service.impl;

import com.alibaba.druid.sql.visitor.functions.Substring;
import com.fan.hospital.mapper.UserMainMapper;
import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.Order;
import com.fan.hospital.pojo.Score;
import com.fan.hospital.pojo.dto.OrderDto;
import com.fan.hospital.pojo.vo.OrderVo;
import com.fan.hospital.service.UserMainService;
import com.fan.hospital.utils.IdWorker;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
public class UserMainServiceImpl implements UserMainService {
    @Autowired
    private UserMainMapper userMainMapper;
    @Autowired
    private IdWorker idWorker;
    @Override
    public List<Doctor> getDoctor(String dept) {
        return userMainMapper.getDoctor(dept);
    }

    @SneakyThrows
    @Override
    public void saveOrder(OrderDto orderDto) {
        String a = orderDto.getDate1().substring(0,11);
        String b = orderDto.getDate2().substring(11,24);
        String c = a+b;
        String myDateString = c.replace("Z","+0000");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
            Date myDate = dateFormat.parse(myDateString);
        long orderId = idWorker.nextId();
        Order order = new Order();
        BeanUtils.copyProperties(orderDto, order);
        String orderDept = orderDto.getDept();
        order.setOrderId(orderId);
        order.setOrderTime(myDate);
        order.setOrderDept(orderDept);
        order.setOrderTime(myDate);
        String doctor = orderDto.getDoctor();
        Doctor doc = userMainMapper.getDoctorByDoctorId(doctor);
        order.setOrderDoctor(doc.getUserName());
        order.setDoctorId(doc.getDoctorId());
        order.setDoctorGrade(doc.getDoctorGrade());
        order.setCreateTime(new Date());
        userMainMapper.saveOrder(order);
    }

    @Override
    public Doctor getDoctorByDoctorId(String doctorName) {
        return userMainMapper.getDoctorByDoctorId(doctorName);
    }

    /**
     * 根据userId获取订单信息
     */
    @Override
    public List<OrderVo> getUserOrder(long userId) {
        return userMainMapper.getUserOrder(userId);
    }

    @Override
    public void deleteOrderById(long orderId) {
        userMainMapper.deleteOrderById(orderId);
    }

    @Override
    public List<Doctor> getAllDoctor() {
        return userMainMapper.getAllDoctor();
    }

    @Override
    public List<Doctor> getDoctorByName(String username) {
        return userMainMapper.getDoctorByName(username);
    }

    @Override
    public void updateOrder(OrderVo orderVo) {
        userMainMapper.updateOrder(orderVo);
    }

    @Override
    public void saveScore(Score score) {
        userMainMapper.saveScore(score);
    }
}
