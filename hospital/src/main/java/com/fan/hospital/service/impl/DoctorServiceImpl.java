package com.fan.hospital.service.impl;

import com.fan.hospital.mapper.DoctorMapper;
import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.service.DoctorService;
import com.fan.hospital.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoctorServiceImpl implements DoctorService {
    @Autowired
    private DoctorMapper doctorMapper;
    @Autowired
    private IdWorker idWorker;
    @Override
    public void save(Doctor doctor) {
        long doctorId = idWorker.nextId();
        doctor.setDoctorId(doctorId);
        doctorMapper.save(doctor);
    }
    @Override
    public Doctor getByUsername(String username) {
        return doctorMapper.getByUsername(username);
    }
    @Override
    public void updatePwd(Doctor doctor) {
        doctorMapper.updatePwd(doctor);
    }

    @Override
    public void update(Doctor doctor) {
        doctorMapper.update(doctor);
    }


}
