package com.fan.hospital.service.impl;

import com.fan.hospital.mapper.UserMapper;
import com.fan.hospital.pojo.User;
import com.fan.hospital.service.UserService;
import com.fan.hospital.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IdWorker idWorker;
    @Override
    public void save(User user) {
        user.setUserId(idWorker.nextId());
        userMapper.save(user);
    }
    @Override
    public User getByUsername(String username) {
        return userMapper.getByUsername(username);
    }
    @Override
    public void updatePwd(User user) {
        userMapper.updatePwd(user);
    }

    @Override
    public void update(User user) {
        userMapper.update(user);
    }


}
