package com.fan.hospital.service.impl;

import com.fan.hospital.mapper.SysUserMapper;
import com.fan.hospital.pojo.SysUser;
import com.fan.hospital.exception.HospitalException;
import com.fan.hospital.pojo.dto.UpdatePwdDto;
import com.fan.hospital.service.SysUserService;
import com.fan.hospital.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统用户实现类
 * @author ：naruto
 * @version 1.0
 * @date ：Created in 2021/7/22 10:35
 * @modified By：
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    @Override
    public SysUser querySysUserByName(String username) {
        return sysUserMapper.querySysUserByName(username);
    }

    @Override
    public void update(SysUser sysUser) {
        sysUserMapper.update(sysUser);
        ShiroUtils.setUser(sysUser);
    }

    @Override
    public void updatePwd(UpdatePwdDto updatePwdDto) {
        // 根据id查询用户
        SysUser sysUser = sysUserMapper.getAllById(updatePwdDto.getId());
        // 比对当前密码
        if (!updatePwdDto.getCurrentPwd().equals(sysUser.getPassWord())) {
            throw new HospitalException("当前密码不正确！");
        }
        // 修改新密码
        sysUserMapper.updatePwd(updatePwdDto);
    }

    @Override
    public SysUser getSysUser() {
        return sysUserMapper.getSysUser();
    }


}
