package com.fan.hospital.service;

import com.fan.hospital.pojo.User;

public interface UserService {
    /**
     * 保存
     * @param user
     */
    void save(User user);
    /**
     * 根据用户名查询
     * @param username
     * @return
     */
    User getByUsername(String username);
    /**
     * 修改密码
     * @param user
     */
    void updatePwd(User user);

    /**
     * 修改用户
     * @param user
     */
    void update(User user);


}
