package com.fan.hospital.shiro;
import com.fan.hospital.utils.StringUtils;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.context.annotation.Configuration;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;
import java.util.UUID;
/**
 * 重写DefaultWebSessionManager
 * @author ：naruto
 * @version 1.0
 * @date ：Created in 2021/7/23 9:27
 * @modified By：
 */
@Configuration
public class TokenWebSessionManager extends DefaultWebSessionManager {
    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
//从请求头中获取token
        String token = WebUtils.toHttp(request).getHeader("Authorization");
//如果token存在，就返回token，否则就生成一个
        if(StringUtils.isNotBlank(token)) {
            return token;
        }
        return UUID.randomUUID().toString();
    }
}
