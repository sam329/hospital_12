package com.fan.hospital.pojo;

import com.fan.hospital.shiro.LoginUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Order implements Serializable {
    private long orderId;
    private String userName;
    private long userId;
    private long phone;
    private int age;
    private String sex;
    private String orderDept;
    private String orderDoctor;
    private long doctorId;
    private String doctorGrade;
    private Date orderTime;
    private String pay;
    private String money;
    private String name;
    private String detail;
    private Date createTime;

}
