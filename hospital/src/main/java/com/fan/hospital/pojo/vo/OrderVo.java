package com.fan.hospital.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class OrderVo implements Serializable {
    private long orderId;
    private int age;
    private String sex;
    private String userName;
    private long userId;
    private String orderDept;
    private String orderDoctor;
    private long doctorId;
    private String doctorGrade;
    private String orderTime;
    private String pay;
    private String money;
    private String name;
    private long phone;
    private String detail;
    private String createTime;

}
