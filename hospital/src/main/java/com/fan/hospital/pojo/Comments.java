package com.fan.hospital.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class Comments implements Serializable {
    private long id;//留言id
    private long userId;
    private long doctorId;
    private String userName;
    private String content;//用户留言内容
    private Date time;//留言时间
    private String reply;//医生回复留言
    private Date replyTime;//回复留言时间

}
