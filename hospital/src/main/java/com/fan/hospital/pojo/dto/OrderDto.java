package com.fan.hospital.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto implements Serializable {
    private long orderId;
    private int age;
    private String sex;
    private String userName;
    private long userId;
    private long phone;
    private String dept;
    private String doctor;
    private long doctorId;
    private String doctorGrade;
    private String date1;
    private String date2;
    private String pay;
    private String money;
    private String name;
    private String detail;

}
