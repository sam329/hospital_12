package com.fan.hospital.mapper;

import com.fan.hospital.pojo.Comments;
import com.fan.hospital.pojo.vo.CommentsVo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DoctorMainMapper {
    List<CommentsVo> getCommentsByDoctorId(long doctorId);
    void updateCommentsReply(Comments comments);

}
