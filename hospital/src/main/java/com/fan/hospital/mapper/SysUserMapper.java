package com.fan.hospital.mapper;

import com.fan.hospital.pojo.SysUser;
import com.fan.hospital.pojo.dto.UpdatePwdDto;
import org.springframework.stereotype.Component;

/**
 * 系统用户
 */
@Component
public interface SysUserMapper {


    SysUser querySysUserByName(String username);
    /**
     * 修改
     * @param sysUser
     */
    void update(SysUser sysUser);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    SysUser getAllById(Long id);

    /**
     * 修改密码
     * @param updatePwdDto
     */
    void updatePwd(UpdatePwdDto updatePwdDto);

    /**
     * 获取管理员信息
     * @return
     */
    SysUser getSysUser();


}

