package com.fan.hospital.controller;

import com.fan.hospital.enums.ResultEnum;
import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.pojo.DoctorSh;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.vo.OrderVo;
import com.fan.hospital.service.DoctorMainService;
import com.fan.hospital.service.SysUserMainService;
import com.fan.hospital.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sysUserMain")
public class SysUserMainController {
    @Autowired
    private SysUserMainService sysUserMainService;
    /**
     * 获取所有用户信息
     */
    @RequestMapping(value = "/getAllUser", method = RequestMethod.GET)
    public Result<?> getAllUser() {
        ;
        return new Result<>(sysUserMainService.getAllUser());
    }

    /**
     * byName查询用户信息
     */
    @RequestMapping(value = "/getUserByName/{username}", method = RequestMethod.POST)
    public Result<?> getUserByName(@PathVariable String username) {
        return new Result<>(sysUserMainService.getUserByName(username));
    }

    /**
     * 修改用户信息
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
    public Result<?> updateUser(@RequestBody User user) {
        sysUserMainService.updateUser(user);
        return new Result<>("操作成功");
    }

    /**
     * byId查找用户信息
     */
    @RequestMapping(value = "/getByUserId/{userId}", method = RequestMethod.POST)
    public Result<?> getByUserId(@PathVariable long userId) {
        return new Result<>(sysUserMainService.getByUserId(userId));
    }

    /**
     * byId删除用户信息
     */
    @RequestMapping(value = "/deleteUserById/{userId}", method = RequestMethod.POST)
    public Result<?> deleteUserById(@PathVariable long userId) {
        sysUserMainService.deleteUserById(userId);
        return new Result<>(ResultEnum.SUCCESS);
    }

    /**
     * 获取所有医生信息
     */
    @RequestMapping(value = "/getAllDoctor", method = RequestMethod.GET)
    public Result<?> getAllDoctor() {
        return new Result<>(sysUserMainService.getAllDoctor());
    }

    /**
     * byName查医生信息，在userMainController
     * /userMain/getDoctor
     */

    /**
     * 修改医生信息updateDoctor
     */
    @RequestMapping(value = "/updateDoctor", method = RequestMethod.PUT)
    public Result<?> updateDoctor(@RequestBody Doctor doctor) {
        sysUserMainService.updateDoctor(doctor);
        return new Result<>(ResultEnum.SUCCESS);
    }

    /**
     * 删除医生信息
     */
    @RequestMapping(value = "/deleteDoctorById/{doctorId}", method = RequestMethod.POST)
    public Result<?> deleteDoctorById(@PathVariable long doctorId) {
        sysUserMainService.deleteDoctorById(doctorId);
        return new Result<>(ResultEnum.SUCCESS);
    }

    /**
     * 获取所有订单信息
     */
    @RequestMapping(value = "/getAllOrder", method = RequestMethod.GET)
    public Result<?> getAllOrder() {

        return new Result<>(sysUserMainService.getAllOrder());
    }

    /**
     * byName(患者姓名)查找订单信息
     */

    /**
     * 修改订单信息updateOrder
     */
    @RequestMapping(value = "/updateOrder", method = RequestMethod.PUT)
    public Result<?> updateOrder(@RequestBody OrderVo orderVo) {
        sysUserMainService.updateOrder(orderVo);
        return new Result<>(ResultEnum.SUCCESS);
    }


    /**
     *  根据orderId删除订单，在userMainController
     * /userMain/deleteOrderById
     */

    /**
     * 获取所有医生申请注册信息
     */
    @RequestMapping(value = "/getAllDoctorSh", method = RequestMethod.GET)
    public Result<?> getAllDoctorSh() {
        return new Result<>(sysUserMainService.getAllDoctorSh());
    }

    /**
     * 同意1医生申请注册信息
     */
    @RequestMapping(value = "/agreeDoctorSh", method = RequestMethod.PUT)
    public Result<?> agreeDoctorSh(@RequestBody DoctorSh doctorSh) {
        sysUserMainService.agreeDoctorSh(doctorSh);
        return new Result<>(ResultEnum.SUCCESS);
    }

    /**
     * 拒绝2医生申请注册信息
     */
    @RequestMapping(value = "/refuseDoctorSh/{doctorId}", method = RequestMethod.POST)
    public Result<?> refuseDoctorSh(@PathVariable long doctorId) {
        sysUserMainService.refuseDoctorSh(doctorId);
        return new Result<>(ResultEnum.SUCCESS);
    }
}
