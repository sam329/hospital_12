package com.fan.hospital.controller;

import com.fan.hospital.enums.ResultEnum;
import com.fan.hospital.pojo.SysUser;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.dto.UpdatePwdDto;
import com.fan.hospital.pojo.vo.TokenVo;
import com.fan.hospital.service.SysUserService;
import com.fan.hospital.shiro.LoginToken;
import com.fan.hospital.shiro.LoginUser;
import com.fan.hospital.utils.Result;
import com.fan.hospital.utils.ShiroUtils;
import com.fan.hospital.enums.UserTypeEnum;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * 用户
 * @author ：naruto
 * @version 1.0
 * @date ：Created in 2021/7/22 9:41
 * @modified By：
 */
@RestController
@RequestMapping("/sysUser")
public class SysUserController {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private SysUserService sysUserService;
    /**
     * 登录
     *
     * @param sysUser
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result<TokenVo> login(@RequestBody SysUser sysUser) {
        Subject subject = SecurityUtils.getSubject();
        AuthenticationToken authenticationToken =
                new LoginToken(sysUser.getUserName(), sysUser.getPassWord(), UserTypeEnum.SYS_USER);
        try {
            subject.login(authenticationToken);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(ResultEnum.LOGIN_PARAM_ERROR);
        }
        Serializable token = subject.getSession().getId();
        return new Result<>(new TokenVo(token));
    }


    /**
     * 获取当前登录用户
     *
     * @return
     */
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public Result<LoginUser> info() {
        LoginUser sysUser = ShiroUtils.getLoginUser();
        //sysUser.setPassword(null);
        return new Result<>(sysUser);
    }
    /**
     * 退出登录
     *
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public Result<?> logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return new Result<>("退出成功！");
    }
    /**
     * 修改管理员
     * @param sysUser
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Result<?> update(@RequestBody SysUser sysUser) {
        sysUserService.update(sysUser);
        return new Result<>("修改成功");
    }

    /**
     * 修改密码
     * @param updatePwdDto
     * @return
     */
    @RequestMapping(value = "/updatePwd", method = RequestMethod.PUT)
    public Result<?> updatePwd(@RequestBody UpdatePwdDto updatePwdDto) {
        sysUserService.updatePwd(updatePwdDto);
        return new Result<>("修改成功");
    }
    /**
     * 获取管理员信息
     * @return
     */
    @RequestMapping(value = "/getSysUser", method = RequestMethod.GET)
    public Result<SysUser> getSysUser() {
        SysUser sysUser = sysUserService.getSysUser();
        return new Result<>(sysUser);
    }


}

