package com.fan.hospital.controller;

import com.fan.hospital.pojo.Comments;
import com.fan.hospital.pojo.Doctor;
import com.fan.hospital.service.DoctorMainService;
import com.fan.hospital.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/doctorMain")
public class DoctorMainController {
    @Autowired
    private DoctorMainService doctorMainService;
    /**
     * 根据doctorId获取留言
     */
    @RequestMapping(value = "/getCommentsByDoctorId/{doctorId}", method = RequestMethod.POST)
    public Result<?> getCommentsByDoctorId(@PathVariable long doctorId) {
        return new Result<>(doctorMainService.getCommentsByDoctorId(doctorId));
    }

    /**
     * 医生回复留言，更新留言表
     */
    @RequestMapping(value = "/updateCommentsReply", method = RequestMethod.POST)
    public Result<?> updateCommentsReply(@RequestBody Comments comments) {
        doctorMainService.updateCommentsReply(comments);
        return new Result<>("回复成功");
    }
}
