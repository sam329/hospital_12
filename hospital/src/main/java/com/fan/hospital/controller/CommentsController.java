package com.fan.hospital.controller;

import com.fan.hospital.enums.ResultEnum;
import com.fan.hospital.pojo.Comments;
import com.fan.hospital.service.CommentsService;
import com.fan.hospital.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comments")
public class CommentsController {
    @Autowired
    private CommentsService commentsService;

    // 保存用户留言
    @RequestMapping(value = "/saveComments", method = RequestMethod.POST)
    public Result<?> saveComments(@RequestBody Comments comments) {
        commentsService.saveComments(comments);
        return new Result<>(ResultEnum.SUCCESS);
    }

    // 查找用户留言
    @RequestMapping(value = "/getLiuYanJL/{userId}", method = RequestMethod.POST)
    public Result<?> getLiuYanJL(@PathVariable long userId) {
        return new Result<>( commentsService.getLiuYanJL(userId));
    }

    // 保存回复留言

    //查找回复留言

}
