package com.fan.hospital.controller;

import com.fan.hospital.enums.ResultEnum;
import com.fan.hospital.pojo.Score;
import com.fan.hospital.pojo.User;
import com.fan.hospital.pojo.dto.OrderDto;
import com.fan.hospital.pojo.vo.OrderVo;
import com.fan.hospital.service.UserMainService;
import com.fan.hospital.shiro.LoginUser;
import com.fan.hospital.utils.Result;
import com.fan.hospital.utils.ShiroUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/userMain")
public class UserMainController {
    @Autowired
    private UserMainService userMainService;
    /**
     * 根据科室查找医生信息
     */
    @RequestMapping(value = "/getDoctor/{dept}", method = RequestMethod.POST)
    public Result<?> getDoctor(@PathVariable String dept) {
        return new Result<>( userMainService.getDoctor(dept));
    }

    /**
     * 提交预约信息
     */
    @RequestMapping(value = "/saveOrder", method = RequestMethod.POST)
    public Result<?> saveOrder(@RequestBody OrderDto orderDto) {
        userMainService.saveOrder(orderDto);
        return new Result<>(ResultEnum.SUCCESS);
    }

    /**
     * 根据userId查找订单信息
     */
    @RequestMapping(value = "/getUserOrder/{userId}", method = RequestMethod.POST)
    public Result<?> getUserOrder(@PathVariable long userId) {
        return new Result<>( userMainService.getUserOrder(userId));
    }

    /**
     * 根据orderId删除订单
     */
    @RequestMapping(value = "/deleteOrderById/{orderId}", method = RequestMethod.POST)
    public Result<?> deleteOrderById(@PathVariable long orderId) {
        userMainService.deleteOrderById(orderId);
        return new Result<>(ResultEnum.SUCCESS);
    }

    /**
     * 显示所有医生
     */
    @RequestMapping(value = "/getAllDoctor", method = RequestMethod.GET)
    public Result<?> getAllDoctor() {
        return new Result<>(userMainService.getAllDoctor());
    }

    /**
     * 根据姓名模糊查询医生
     */
    @RequestMapping(value = "/getDoctorByName/{username}", method = RequestMethod.POST)
    public Result<?> getDoctorByName(@PathVariable String username) {
        return new Result<>(userMainService.getDoctorByName(username));
    }

    //更新订单表
    @RequestMapping(value = "/updateOrder", method = RequestMethod.PUT)
    public Result<?> updateOrder(@RequestBody OrderVo orderVo) {
        userMainService.updateOrder(orderVo);
        return new Result<>("操作成功");
    }

    /**
     * 保存评分
     */
    @RequestMapping(value = "/saveScore", method = RequestMethod.POST)
    public Result<?> saveScore(@RequestBody Score score) {
        userMainService.saveScore(score);
        return new Result<>(ResultEnum.SUCCESS);
    }
}
