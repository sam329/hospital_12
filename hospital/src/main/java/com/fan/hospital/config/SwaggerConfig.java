package com.fan.hospital.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

//@Configuration用于定义配置类,可替换xml配置文件,被注解的类内部包含有一个或多个被@Bean注解的方法
@Configuration
@EnableSwagger2   //开取Swagger2
public class SwaggerConfig {
    //配置了Swagger的Docket的bean实例
    @Bean
    public Docket docket(){

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("ffl886")//设置API文档的分组名称
                //enable是否启动Swagger，如果为False，则Swagger不能再浏览器中访问
                .enable(true)
                .select()
                //根据包路径扫描接口
                .apis(RequestHandlerSelectors.basePackage("com.fan.hospital.controller"))
                .build();
    }

    //配置Swagge信息=apiInfo
    private  ApiInfo apiInfo(){
        //作者信息
        Contact contact = new Contact("范", "https://blog.csdn.net/ffl886?spm=1001.2101.3001.5343", "717253612@qq.com");
        return new ApiInfo(
                "ffl886的SwaggerAPI文档",
                "佛曰不可说",
                "1.0",
                "https://blog.csdn.net/ffl886?spm=1001.2101.3001.5343",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());

    }
}
